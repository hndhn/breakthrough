public class BreakthroughImpl implements Breakthrough {
	
	// create chessboard
	private int[][] chessboard = new int[8][8];
	// white player go first
	private PlayerType player = PlayerType.WHITE;

	public BreakthroughImpl() {
		setUpchessboard();
	}
	// 1: black, 2: white, 0: empty
	public void setUpchessboard() {
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				if (i < 2) {
					chessboard[i][j] = 1;
				} else if (i > 5) {
					chessboard[i][j] = 2;
				} else {
					chessboard[i][j] = 0;
				}
			}
		}
	}

	public PieceType getPieceAt(int row, int column) {
		int p = chessboard[row][column];
		if (p == 1) {
			return PieceType.BLACK;
		} else if (p == 2)
			return PieceType.WHITE;
		return PieceType.NONE;
	}

	public PlayerType getPlayerInTurn() {
		return player;
	}

	public PlayerType getWinner() {
		for (int i = 0; i < 8; i++) {
			if (getPieceAt(0, i) == PieceType.WHITE)
				return PlayerType.WHITE;
			else if (getPieceAt(7, i) == PieceType.BLACK)
				return PlayerType.BLACK;
		}
		return null;
	}

	public boolean isMoveValid(int fromRow, int fromColumn, int toRow, int toColumn) {
		if (!isPlayerTurn(fromRow, fromColumn))
			return false;
		if (!isSpotValid(toRow, toColumn))
			return false;
		if (!moveNextTo(fromRow, fromColumn, toRow, toColumn))
			return false;
		if (!isBlocked(fromRow, fromColumn, toRow, toColumn))
			return false;

		return true;
	}

	public boolean isPlayerTurn(int fromRow, int fromColumn) {
		if (player == PlayerType.BLACK && getPieceAt(fromRow, fromColumn) != PieceType.BLACK)
			return false;
		if (player == PlayerType.WHITE && getPieceAt(fromRow, fromColumn) != PieceType.WHITE)
			return false;
		return true;
	}

	public boolean isSpotValid(int toRow, int toColumn) {
		if (player == PlayerType.BLACK && (toColumn < 0 || toRow < 0))
			return false;
		if (player == PlayerType.WHITE && (toColumn > 7 || toRow > 7))
			return false;
		return true;
	}

	public boolean moveNextTo(int fromRow, int fromColumn, int toRow, int toColumn) {
		if (Math.abs(toRow - fromRow) > 1)
			return false;
		if (Math.abs(toColumn - fromColumn) > 1)
			return false;
		return true;
	}

	public boolean isBlocked(int fromRow, int fromColumn, int toRow, int toColumn) {
		if (toColumn == fromColumn && chessboard[toRow][toColumn] != 0)
			return false;
		return true;
	}

	public void move(int fromRow, int fromColumn, int toRow, int toColumn) {
		if (isMoveValid(fromRow, fromColumn, toRow, toColumn)) {
			chessboard[toRow][toColumn] = chessboard[fromRow][fromColumn];
			chessboard[fromRow][fromColumn] = 0;
			if (player == PlayerType.BLACK) player = PlayerType.WHITE;
			else player = PlayerType.BLACK;
			getWinner();
		}
	}
  
}

