import org.junit.*;

import static org.junit.Assert.*;

public class TestBreakthrough {
	Breakthrough game;
	@Before
	public void setUp() {
		game = new BreakthroughImpl();
	}

	@Test
	public void CheckFirstPlayer(){
		assertEquals( "First player is white",
				BreakthroughImpl.PlayerType.WHITE, game.getPlayerInTurn());
	}
	@Test
	public void CheckWhitePawn(){
		assertEquals( "White pawn in (7,7)",
				BreakthroughImpl.PieceType.WHITE, game.getPieceAt(7,7));
	}
	@Test
	public void CheckEmpty(){
		assertEquals( "Empty in (5,3)",
				BreakthroughImpl.PieceType.NONE, game.getPieceAt(5,3));
	}
	
	@Test
	public void CheckLegalMove(){
		assertTrue("Move Straight",game.isMoveValid(6,2,5,2));
		assertTrue("Move diagonal right",game.isMoveValid(6,2,5,3));
	}
	@Test
	public void IllegalMove(){
		assertFalse("White cannot capture white",game.isMoveValid(7,0,6,0));
	}

	@Test
	public void BlackCaptureWhite(){
		game.move(6, 0, 5, 0);
		game.move(1, 0, 2, 0);
		game.move(5, 0, 4, 1);
		game.move(2, 0, 3, 0);
		assertTrue("Black captures White",game.isMoveValid(4, 1, 3, 0));
	}

}